//F1-12键
//--------------------------------------------------------------------------------------------------------------------------------------------
keys['F1'] = function(){document.getElementById("cmd_newNavigatorTab").doCommand();}; //新建标签页并光标定位到地址栏
keys['F2'] = function() {var oldHistory = gBrowser.webNavigation.sessionHistory;
gBrowser.selectedTab = gBrowser.addTab("about:blank");
var newHistory = gBrowser.webNavigation.sessionHistory;
newHistory.QueryInterface(Components.interfaces.nsISHistoryInternal);
for (var i = 0; i < oldHistory.count; i++) { newHistory.addEntry(oldHistory.getEntryAtIndex(i, false), true); }
if(oldHistory.count) gBrowser.webNavigation.gotoIndex(oldHistory.index);}; //复制当前标签页
keys['F3'] = function(ev) {BrowserPageInfo();}; //查看页面信息
keys['F4'] ="gBrowser.selectedTab.toggleMuteAudio()"; // 关闭当前标签声音
keys['F6'] ="RandomIMG();"; //切换壁纸
keys['F8'] =function() { gBrowser.loadURI("javascript:%20void((function()%20{var%20element%20=%20document.createElement('script');element.id%20=%20'outfox_seed_js';element.charset%20=%20'utf-8',element.setAttribute('src',%20'http://fanyi.youdao.com/web2/seed.js?'%20+%20Date.parse(new%20Date()));document.body.appendChild(element);})())");}; //启用翻译功能
keys['F9'] = function(){var newtabs=["http://weibo.com/","https://feedly.com","https://www.twitter.com/","http://www.runningcheese.com/"];var i=0;while(i<=newtabs.length-1){gBrowser.selectedTab=gBrowser.addTab(newtabs[i]);i=i+1;}};//一键打开标签组
//keys['F10'] = function () {loadURI(PlacesUtils.getURLAndPostDataForKeyword("aa")[0]);}; //以书签的关键词执行书签（主要用于Bookmarkets）


//Alt组合键
//--------------------------------------------------------------------------------------------------------------------------------------------
keys["Alt+F1"] = function(){var tabs = gBrowser.mTabContainer.childNodes,i; for (i = 0; tabs[i] != gBrowser.selectedTab; i++);	for (i--;i>=0;i--){gBrowser.removeTab(tabs[i]);}};  //关闭左侧所有标签页
keys["Alt+F2"] = function(){var tabs = gBrowser.mTabContainer.childNodes; for (var i = tabs.length - 1; tabs[i] != gBrowser.selectedTab; i--) { gBrowser.removeTab(tabs[i]);}};  //关闭右侧所有标签页
keys["Alt+F3"] = "gBrowser.removeAllTabsBut(gBrowser.mCurrentTab);";  //关闭其他标签页
keys['Alt+`'] = function(){var tab = gBrowser.tabContainer.selectedItem;if (tab.pinned) gBrowser.unpinTab(tab);else gBrowser.pinTab(tab);};  //Pin/UnPin当前标签
keys['Alt+1'] = "gBrowser.mTabContainer.advanceSelectedTab(-1,true);";//上一标签
keys['Alt+2'] = "gBrowser.mTabContainer.advanceSelectedTab(1,true);";//下一标签
keys['Alt+A'] = "gBrowser.selectedBrowser.messageManager.sendAsyncMessage('FGgTranslator', readFromClipboard());";//开启选中文字翻译
keys["Alt+S"] = function(){var id = [12]; var service = Components.classes["@userstyles.org/style;1"].getService(Components.interfaces.stylishStyle); for (var i=0; i < id.length; i++){var style = service.find(id[i], service.REGISTER_STYLE_ON_CHANGE); style.enabled = !style.enabled; style.save();}};//夜晚模式
keys["Alt+D"] = "QNbrowser.openQuickNote();"//打开笔记本
keys['Alt+W'] = "RIL.hotKeyToggle();"//稍后阅读标记/去除标记
keys['Alt+E'] = "ReaderParent.toggleReaderMode(event);";//阅读模式
keys['Alt+R'] = function () { if (window.windowState === 1) {restore();} else if (window.windowState === 2) {restore();} else if (window.windowState === 3) {maximize();}
else if (window.windowState === 4) {restore();} else {} };//切换窗口大小
keys['Alt+T'] = function () { window.open("http://m.iciba.com/","爱词霸翻译","resizable,scrollbars,status,title").resizeTo(420, 670); };//打开翻译窗口
keys['Alt+G'] = "var s = prompt('谷歌站内搜索:', '');if (s.length > 0) gBrowser.addTab('https://www.google.com/search?q=site:' + encodeURIComponent(content.location.host) + ' ' + encodeURIComponent(s));";//Google站内搜索
keys['Alt+B'] = "var s = prompt('百度站内搜索:', '');if (s.length > 0) gBrowser.addTab('https://www.baidu.com/s?wd=site:' + encodeURIComponent(content.location.host) + ' ' + encodeURIComponent(s));";//Baidu站内搜索
keys['Alt+X'] = "getWebNavigation().canGoForward && getWebNavigation().goForward();";//前进
keys['Alt+Z'] = "getWebNavigation().canGoBack && getWebNavigation().goBack();";//后退
keys['Alt+C'] = function () { var charset = gBrowser.mCurrentBrowser._docShell.charset; BrowserSetForcedCharacterSet(charset == "UTF-8" ? "GBK" : "UTF-8");  }//切换文字编码
keys['Alt+V'] = function () { if (gPrefService.getPrefType("general.useragent.override") === 0) {ucjs_UAChanger.setUA(8);} else {ucjs_UAChanger.setUA(0); } };//切换用户代理
keys['Alt+U'] = function(ev) {BrowserPageInfo();}; //查看页面信息
keys['Alt+I'] = function() {
	try {
		var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProgF", Components.interfaces.nsILocalFile);
		file.append("Internet Explorer");
		file.append("iexplore.exe");
		var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
		process.init(file);
		process.run(false, [content.location.href], 1);
	} catch (ex) {
		alert("\u6253\u5f00IE\u5931\u8d25!")
	}
}; //用IE打开当前页
keys['Alt+O'] = "openPreferences();"; //Firefox选项
keys['Alt+P'] = "OpenBrowserWindow({private: true});"; //打开隐私窗口





//Ctrl+Alt组合键
//--------------------------------------------------------------------------------------------------------------------------------------------

keys['Ctrl+Alt+A'] =  function() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\snapshot.exe"); file.launch(); return file;};//截图
keys['Ctrl+Shift+Alt+A'] =  function() {document.getElementById("titlebar-min").click(); var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\snapshot.exe"); file.launch(); return file;};//隐藏火狐截图
keys['Ctrl+Alt+Q'] =  function() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\FastStone\\FSCapture.exe"); file.launch(); return file;};//完整截图
keys["Ctrl+Alt+S"]  = function() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\ScreenGif.exe"); file.launch(); return file;}; //GIF截图
keys["Ctrl+Alt+F"]  = function() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\Everything.exe"); file.launch(); return file;}; //搜索本机文件




//音乐播放器快捷键：
//--------------------------------------------------------------------------------------------------------------------------------------------
keys["Ctrl+VK_DOWN"] = "SimpleMusicPlayer.doAction('playPause');";//暂停or开始快捷键
keys["Alt+VK_UP"] = "SimpleMusicPlayer.doAction('play');";//开始快捷键
keys["Alt+VK_DOWN"] = "SimpleMusicPlayer.doAction('pause');";//暂停快捷键
keys["Alt+VK_LEFT"] = "SimpleMusicPlayer.doAction('prev');";//上一曲
keys["Alt+VK_RIGHT"] = "SimpleMusicPlayer.doAction('next');";//下一曲
keys["Alt+VK_INSERT"] = "SimpleMusicPlayer.doAction('love');";//喜欢
keys["Alt+VK_DELETE"] = "SimpleMusicPlayer.doAction('hate');";//讨厌


