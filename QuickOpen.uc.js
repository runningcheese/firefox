// ==UserScript==
// @name          QuickOpen.uc.js
// @description   QuickOpen 快速打开指定选项
// @author         Runningcheese
// @namespace   http://www.runningcheese.com
// @include        main
// @license         MIT License
// @compatibility  Firefox 29+
// @charset        UTF-8
// @version        v2017.06.17 
// @homepage    http://www.runningcheese.com/firefox-v8
// ==/UserScript==

//载入脚本
function jsonToDOM(json, doc, nodes) {

    var namespaces = {
        html: 'http://www.w3.org/1999/xhtml',
        xul: 'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul'
    };
    var defaultNamespace = namespaces.html;

    function namespace(name) {
        var m = /^(?:(.*):)?(.*)$/.exec(name);        
        return [namespaces[m[1]], m[2]];
    }

    function tag(name, attr) {
        if (Array.isArray(name)) {
            var frag = doc.createDocumentFragment();
            Array.forEach(arguments, function (arg) {
                if (!Array.isArray(arg[0]))
                    frag.appendChild(tag.apply(null, arg));
                else
                    arg.forEach(function (arg) {
                        frag.appendChild(tag.apply(null, arg));
                    });
            });
            return frag;
        }

        var args = Array.slice(arguments, 2);
        var vals = namespace(name);
        var elem = doc.createElementNS(vals[0] || defaultNamespace, vals[1]);

        for (var key in attr) {
            var val = attr[key];
            if (nodes && key == 'id')
                nodes[val] = elem;

            vals = namespace(key);
            if (typeof val == 'function')
                elem.addEventListener(key.replace(/^on/, ''), val, false);
            else
                elem.setAttributeNS(vals[0] || '', vals[1], val);
        }
        args.forEach(function(e) {
            try {
                elem.appendChild(
                                    Object.prototype.toString.call(e) == '[object Array]'
                                    ?
                                        tag.apply(null, e)
                                    :
                                        e instanceof doc.defaultView.Node
                                        ?
                                            e
                                        :
                                            doc.createTextNode(e)
                                );
            } catch (ex) {
                elem.appendChild(doc.createTextNode(ex));
            }
        });
        return elem;
    }
    return tag.apply(null, json);
}


//定义按钮
CustomizableUI.createWidget({
    id: 'QuickOpen',
    defaultArea: CustomizableUI.AREA_NAVBAR,
    label: '快速打开',
    tooltiptext: '快速打开指定选项',
    onCreated: function(aNode) {
    aNode.setAttribute('type', 'menu');    
        
 //定义菜单      
        var myMenuJson = 
                                ['xul:menupopup', {id: 'QuickOpen_pop', position:'after_end'},
                                ['xul:menuitem', {label: '我的电脑',oncommand: 'QuickOpenMyComputer();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAApElEQVQ4jcWTIQ7CYAyF/0vgOQH6T/p9DskNCJ5wAySWC3CDSSQnwHCB+XkQc7ghRlAbWcIymlS99jXt60tpilBnagCbiDgAhXoDHp+inPMcWEbEVj0C55RSAmq16csEXNRnJ9hO721Wm6SWveAkBED1X4IxNL7/ukKvzkNV6PyBSQm+XngIwQJYATtbDxTAFahGkTHnPFcjItYRsVdPb/+UQP0CUEY7Z3H6hLEAAAAASUVORK5CYII='}],
                                ['xul:menuitem', {label: '搜索文件',oncommand: 'EverythingSearch();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4jcXSv0qdQRCG8d9JUmghpBC2sNAIEqshl5D6kE7bkC5WFoJg5zVoAmntRbDJn5sIBEcQAhpIkcDBwkqMGNBmj24+j8Qm5IVlmZnnnZ2B5X+r1wYR8RSf8OQO/hv6mfl1mHjQAdY65kusY6XGs5W5PUFEPMZPjNfUL7zEexw0jc8wlZkn3QleNWZ4kZk7mMJekx+vLHhYX+9hC5MNOFFK+ZyZR6WU49aEmVLKu8FgcD3Bc8z7U8/wIyIW8aFTm6+e6wav3dYmVrGNsRH1JXhUg/4IYGNErlW/bfAbM5n5/S8mEBHT+MLNCvu4qMXL7t2eyp8jqf8gIt7iEG8ycwjd9XoPy5jLzOXhCrv4iI2IuM8Wp1i4D/jvdQWgm0n7Gn2U7gAAAABJRU5ErkJggg=='}],
                                ['xul:menuitem', {label: '管理书签',oncommand: "PlacesCommandHook.showPlacesOrganizer('AllBookmarks');", class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAATElEQVQ4jWNgGGzgPxZMkmZSxAnaTAzGsOEomoKjxLj6P5oAKWEwXA04ysDAYMWACFCSDJjFwMDADmWzQ/kkGUAqoI4BMAbZKZEiAAA7FFJDJW1v5AAAAABJRU5ErkJggg=='}],
                               ['xul:menuitem', {label: '清理痕迹',oncommand: 'Cc["@mozilla.org/browser/browserglue;1"].getService(Ci.nsIBrowserGlue).sanitize(window);',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAb1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABt6r1GAAAAJHRSTlMAzVQUwp9BDPLTp3dwPDUsGPvn39vHtauPh35sZlxMJB736yg/5R5gAAAAhklEQVQY01WPVxLDMAhEJatb1b07TsL9z5goGmckPoB5DAuL7InKoE1VAkLBlESMsKKD8QzVMDagLpITaB3u/mIPCdJRExg+Eqjgyac1Tli/RRDaap5jcxIv02k9/WoY6J6WNPsmvyGLb92BI4H2HrsbvJWovegyF2a5XrhwpTS2hSW+pNc/dQcGVNn7bGYAAAAASUVORK5CYII='}],  
                                 ['xul:menu', {label:'系统工具' ,class:'menu-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABM0lEQVQ4jaWSMUvDUBDH/17lndKIvCpNwFTB0qTUtghNFpMOgsTuDgEnB+nYyQ/QDyEurg79EqKLo906+QE6uLvGoWnM0/pK6R9uuHf3/73jOEAjIhoS0VDXozcLngAYrwzJmctpLIX0hBBTZk6YOcmZ5yoDGM/rQogpgF5WTR+CFYYMUs9MzJwoZWk9kOAJtow3hFcXiwiKR0nqZxGAJItKowvb6WOndAfHj7F3cKMH2E4fhhyBt19Qsp7RjooKcHf/VQ8wq01Yxz7MahPtqIjT80ABzPJ/AK7nAvjKml3PRSuUcD0XlUYXh/WOfgeGHP38tvGJVihJ8ATSfIIh73F0cqsH2LUB7NoAVHiE48dw/FgZP12gfgd5dS6vQZvvoMJHOh0t9Kx9SPh1ysvizymvo2/tuWGnVFMwTgAAAABJRU5ErkJggg=='},
                               ['xul:menupopup', {},
                                ['xul:menuitem', {label: '音量控制',oncommand: 'QuickOpenVolume();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAWUlEQVQ4jWNgoCFQZmBg+E+pZlwG4DUcJolNEbLByoQ0MyAZgFfjfzSsjCbHgEUcwwBy5EgyAN2VKAGMbAC6M4l2AUVhgAzIigV8hpCcDrApxCdPlEsGOQAAKY8zgtP7s6EAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '记事本',oncommand: 'QuickOpenNotedpad();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAANUlEQVQ4jWNgoCL4TwA3EGMAPrnrhAwhZIA4IUMIGcBAyBBCBqBjkgwgSu2oAcPGAFIwdQAA7bk0hAgVKwIAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '设置记事本',oncommand: 'Setnotepad2();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAANUlEQVQ4jWNgoCL4TwA3EGMAPrnrhAwhZIA4IUMIGcBAyBBCBqBjkgwgSu2oAcPGAFIwdQAA7bk0hAgVKwIAAAAASUVORK5CYII='}],
                                ['xul:menuitem', {label: '设置IDM',oncommand: 'SetIDM();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAlUlEQVQ4jWNgoAJYzsDAcB8H/o1Hbj/MgP9E0q/RaJg4w38i8Gso/R6Nz8DAwMCwnoGBQQEHtsEjNxlmwHEGBoYEHHg2HrnNMAPmM5AHGogxQANZITkGMDBAAuw01DCyDNjMAAnx7wwMDA7kGFADNeA6uS7QgOLzUMNINgDZoBxKDEAHcAM2M0AyBjb8Ho/cYTItRgUAX/dLM5/IfscAAAAASUVORK5CYII='}],
                                ['xul:menuitem', {label: '任务管理器',oncommand: 'QuickOpenTaskMGR();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAlUlEQVQ4jWNgoAJYzsDAcB8H/o1Hbj/MgP9E0q/RaJg4w38i8Gso/R6Nz8DAwMCwnoGBQQEHtsEjNxlmwHEGBoYEHHg2HrnNMAPmM5AHGogxQANZITkGMDBAAuw01DCyDNjMAAnx7wwMDA7kGFADNeA6uS7QgOLzUMNINgDZoBxKDEAHcAM2M0AyBjb8Ho/cYTItRgUAX/dLM5/IfscAAAAASUVORK5CYII='}],
                                ['xul:menuseparator', {}],
                                ['xul:menuitem', {label: '计算器',oncommand: 'QuickOpenCALC();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAATklEQVQ4jWNgoCJYw8DA8J8IvAaXAf8ZGBhYCVjCClWH0wAYjQsjq8NrADny1HMBIUCUAbjYJBmA7iWSDMAHhrsXmAloZsZnAMWZiSwAAJGrSHtJfnvHAAAAAElFTkSuQmCC'}],
                                ['xul:menuitem', {label: '命令行',oncommand: 'QuickOpenCMD();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAATklEQVQ4jWNgoBL4TwGGG0CuxbQxQI4SAxQZGBheMzAw2FHiAjuoIWHkGsDAwMCQBxXH5xLauECBATMMMOKdkAsoigVSwCAzgKLMRDYAAKIZQECvdVKSAAAAAElFTkSuQmCC'}],
                                ['xul:menuitem', {label: '放大镜',oncommand: 'QuickOpenMagnify();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAdklEQVQ4jc2SsQ3AIAwEbwGK1NmFDdIwE3OxCCOkZYAU+RSRMEFyIuUkN7Z5vwH4IxtQgKYoyk2RgQokICiScnlmcgWWTm1RbeikaJpFUo9J47RsEdTznYB7BfclwvgZGxCfBC4nvY8UgX1WxOJVkdUj4jp84wDU6yD4kZGU+wAAAABJRU5ErkJggg=='}],
                                ['xul:menuitem', {label: '屏幕键盘',oncommand: 'QuickOpenOSK();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAACVBMVEUAAAAAAAAAAACDY+nAAAAAAnRSTlMAxOqay5EAAAAiSURBVAjXY0ADUqtWrWRQYGBgYlACAgyW1ioIC0xAFKMCABYYBwHv+ZZuAAAAAElFTkSuQmCC'}],
                                ['xul:menuitem', {label: 'IE浏览器',oncommand: 'QuickOpenIE();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABEElEQVQ4jZXSnVuDYRQG8B8EQTAIgkEQBMEgDIIwGA4Hg0EYBMH+gyAIh0EwHIRBEARBEAzDwWAweIP3bD17rmd9nOu64X3ec9/3+eJ/cYpL9DHAwabEHZyjG7jAbQik0SsRHzBHlWGBYebaSclNvEfyFK8FkQoznCSGqxhGwmP0t9ggsBTZS8n7QbjHVSQ9oRUu3UJbN6lAB2O0E+d+CC/xnAnMU4F2uM1+KLuEtRmMs5+fmPyCZlpBrn5mPY7Ud5BiFXl/FUZoJOS8vdGS3PC9+1xkirfC+yJmhnoDFQ7Vq/vL8Ppp+YMYCPVUr+O7RPyQna9Qm2RvLfWh9MKgi2Ns5WTYxot6bXfhsFtKLMUX/+qC1cGn0mkAAAAASUVORK5CYII='}],
                                ['xul:menuitem', {label: 'IE选项设置',oncommand: 'QuickOpenInetcpl();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABYUlEQVQ4ja2SL28CQRDFf7ebzKWChKQJokmTGgwCgUHQoFtT06TmHKKSBEWCQuEwKASCD4BBkCD6AapQoFCnTqAqcRU8YPlX1ZdcsjN7783M24FL3AM9M1uZ2dbMtsA30AFyV/4/wUccxxnQB6rAHZD33jeApcRqt8hvQCqBuUgF59xYuTaQOOfGEj5BAcjMbAU8SWxuZtsoimZh6865LrDQqCfJoS5egXIcx5lzrivRIWBAW/FAvuT3GgvN9gIsgTWQ6C6n9lu6e1B+ou5A5uznWuvzwYhN5R4V1+VLLRTIAXUgBebARG1/npFrQAb0QhOXQBmYyn0DOjqnMhagqsodFTlgwO7ts2CUROSi4oqMHau7VihQBDYHU44LVVJcVtwDRsCG8BmFkWZ9PiOXFL+r+objC12gr3EGZ+TEe9/QuXmLDICqjLRcqcb6AaZA5U+y8BVF0UwCdXaLc7H7/4Zf1MVXuPDScn0AAAAASUVORK5CYII='}],
                               ]
                               ],
                                ['xul:menu', {label:'常用功能' ,class:'menu-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4jaXSIU7EQBSA4S8j+sIFVpD0HIi9wC6CM+0FMFUILA7DDRAIVB0KheoFMKuK2OlmSLZTGl4ymWTe+/95M3lUIqV0SCkdajVVuGmaD/SrJQW8yWtRsouIISLGiBgLeIoN+ikfEQN252w+2K5ocpuZs2CcKbxChx53ZeIXUxF0OOIFDW7XCnq84QnPeF8jaDFkyWvupPuroMUnvgrJvdOfLAomeMh7e+ltNcEjvmvwkuAGD7iegy8J/jdI2JejvLScPne/4sL5+AHddkk+mzKKaQAAAABJRU5ErkJggg=='},
                               ['xul:menupopup', {},
                               ['xul:menuitem', {label: '打开文件',oncommand: 'BrowserOpenFileWindow();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAiUlEQVQ4ja3SsQ2AIBCF4b92CHqnYBx2oHECF9LCWXQGem3ORPHAI/qSyxnEjwsRfooHdqW8FdA2n6gZ6AvrpfL5Rg0o5TFd7aRa3QDzhZUmuMYBE5CkOwWpAjMQgU760gok+Rjp6csEE7C1Ak6QBKzyfmwB8owK8gCCAYnyHHJgoP0nGl4OtOUAFiVQKeOOdswAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '隐私浏览',oncommand: 'OpenBrowserWindow({private: true});',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAyUlEQVQ4je3RIUzDQBjF8V9CMotEoTCYufopBBqLx1ZOYiZn0JVIVC0WXTuJm6mqmpmY6Dt2CYIESXjJ9a7/vn7fvTv+hJ6wRocPTNhnfg+fcMCA1/gfsIIjtmgLiC5SfIPrsCZsiz6F58cvNcInbgJKhBHPlXEd3xgPLLGTnPeBh2T7qh7tMz/GI76+VH+p4JBd1NHK+1A16Mzn5iod7n4KXGmVfy4LaALawAVus7s260W+tYn3reHS+Wom8wF1Gbtkn/AW77+iE6SaONczlmqVAAAAAElFTkSuQmCC'}],
                               ['xul:menuitem', {label: '历史记录',oncommand: 'PlacesCommandHook.showPlacesOrganizer("History");',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA2UlEQVQ4jbXTLVYDMRQF4E9VdANdQA1mXN0sg0VgkcgxyJpqloDCsodYJA5ThRqDQORNG6bJ8HMO95yIebm5k3vfC/+ADe6RMMZKGGJvEdc44oAe61h91I7BaR5+wy6+E65mnC44FyKbUO+K2hNuKj/qgvvFzoD9jHiHh8Zt93JOJyTZZ4kerw2BPs6cMGI1I62ivq0ITHuLAmRrI55x65zRGu8lsWZhwlYO87G49oWFQe7zT3EwC7HWxhaqbeQ8SEsizUEqRf48yhOmx/SCDzntFLVvH9Ov8QntGzLFRkqKRgAAAABJRU5ErkJggg=='}],                        
                               ['xul:menuitem', {label: '故障排除',oncommand: 'var x = gBrowser.mCurrentTab._tPos + 1; gBrowser.moveTabTo(gBrowser.selectedTab =gBrowser.addTab("about:support"), x); ',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAsUlEQVQ4jbXSIQ4CQQyF4S9ZNwaJ4hZ4FII74LEruQASg0buCbDY1XsFDoBCIUFsSUYAu7DwkknaTPu3My0/1gh7NKhRIn0K2eCMJQ4B6oSkqJZDmrCr8N9qgVsWmHANe4pTF2CJY1StsMuSRrh0AR5VxljHWcXdrE8HooNnb03aj9x3AcYR2GAbCVUAekNgrp3IKgDHbyAPFf+EbIdAJnqM9hWkzmAfq9AuXaldsOG6A+IHLLa/+ULtAAAAAElFTkSuQmCC'}],  
                               ['xul:menuitem', {label: '参数设置',oncommand: 'var x = gBrowser.mCurrentTab._tPos + 1; gBrowser.moveTabTo(gBrowser.selectedTab =gBrowser.addTab("about:config"), x); ',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAkElEQVQ4jbWTXQ2AMAyEvzc0TAIWEIIWTMwBBhCBNni5Cw2MwPhp0mT9u7a3DX6UAVikw92iHphCcSs1yKSc02InjkAXYp18Bi6CuPOVeJJiYNS5ATIbB1k+wiSH4iWMndUlSWf5vM6BWBMW7RTsJJ+l3dnvAUorzDUrGOQxifDBNb5+SAZ5/JRLo1Z/pmpZAR6aP0H2Y7uIAAAAAElFTkSuQmCC'}],
                               ['xul:menuitem', {label: '关于about',oncommand: 'var x = gBrowser.mCurrentTab._tPos + 1; gBrowser.moveTabTo(gBrowser.selectedTab =gBrowser.addTab("about:about"), x); ',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAkElEQVQ4jbWTXQ2AMAyEvzc0TAIWEIIWTMwBBhCBNni5Cw2MwPhp0mT9u7a3DX6UAVikw92iHphCcSs1yKSc02InjkAXYp18Bi6CuPOVeJJiYNS5ATIbB1k+wiSH4iWMndUlSWf5vM6BWBMW7RTsJJ+l3dnvAUorzDUrGOQxifDBNb5+SAZ5/JRLo1Z/pmpZAR6aP0H2Y7uIAAAAAElFTkSuQmCC'}],
                               ['xul:menuitem', {label: '编辑user.js',oncommand: 'QuickOpenUserjs();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAR0lEQVQ4jWNgoCL4TwA3EGMAPrnrhAwhZIA4IUMIGcBAyJD/SDQyxiaG1TJ8LiBKLdUMIBSdtHcBxQbgczp9XECRAaRg6gAArMpGck8h/nAAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '删除cookies',oncommand: 'window.openDialog("chrome://browser/content/preferences/cookies.xul");',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAXElEQVQ4jWNgoDaQlZU9LCcn9x8XlpWVPYzXAKhCGxxyNnJycv8xJGRkZB7isxUXlpGReYhhOzHexKmOqgbAnIdOE20AjI1OjxpAggEUxwI+QDsDCOVEJHwEpgcAQdpq5UW7wZYAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '关于浏览器',oncommand: 'openAboutDialog();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABA0lEQVQ4ja2TMW7CQBBFXSUFLlnW2tn3jxAoKHwPciIiUQeuFOoIyBHoDF1SOGlI4ySWsbFkZcrZ/W/2z84kyX9HCGEMrICDpFJSCeyAZQhhfFMcY1wAhaS1meXOudQ5l5pZLmkNnGKMi1vio5nNugqY2Qw4XkGyLHPAqSmWtAVeGpApUHjvJ79JYClp06woaStp25LfAKs6YGdmedfTW6zkwKFOLJMkuWupdJF0aWHcS/ocDHDOpcB7r4UuQGVhXwd0NbEVUM3EXxO99xOgMLNpHyDG+HD1jdXB8EGqXXgEzjHGZ2DuvR9570fAvMqdO8U/UVumN0lfwIekV+Cpd5mGxDdxo1Tjgl/dlwAAAABJRU5ErkJggg=='}],
                               ]
                               ],['xul:menu', {label:'脚本更新' ,class:'menu-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA10lEQVQ4jZ3SoXICQRAE0FdFFQJzEhWFyQfgIqIi+QM8FhkZww+gTyJRsVg0NjIuBoXCRuxc1S1swULLme6enqnhGuNCrRqvOKLF9FmTMb5xxlev/oFJjXiPNQ547/UW+MUKg5J4GOJ5GB0LxAY7bEoGC2n3Lm5bImGEH8wuG3v1h5tHkgxnaY0adCs+bTAMfoYD3ioNpsHPsMT2hmgtPRrpwJ+XhO66y4K4wSk4s5g+Kk2ZhEkr/7ru6qsQv9xIqpFeuJ9kGwk20X8Yg4i+w9+9BPeQif8BV+QnzTuJ0YcAAAAASUVORK5CYII='},
                               ['xul:menupopup', {},
                               ['xul:menuitem', {label: '查看可用更新',oncommand: 'event.stopPropagation(); var x = gBrowser.mCurrentTab._tPos + 1; gBrowser.moveTabTo(gBrowser.selectedTab =gBrowser.addTab("http://git.oschina.net/runningcheese/firefox/raw/master/updatelog.txt"), x);',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABA0lEQVQ4ja2TMW7CQBBFXSUFLlnW2tn3jxAoKHwPciIiUQeuFOoIyBHoDF1SOGlI4ySWsbFkZcrZ/W/2z84kyX9HCGEMrICDpFJSCeyAZQhhfFMcY1wAhaS1meXOudQ5l5pZLmkNnGKMi1vio5nNugqY2Qw4XkGyLHPAqSmWtAVeGpApUHjvJ79JYClp06woaStp25LfAKs6YGdmedfTW6zkwKFOLJMkuWupdJF0aWHcS/ocDHDOpcB7r4UuQGVhXwd0NbEVUM3EXxO99xOgMLNpHyDG+HD1jdXB8EGqXXgEzjHGZ2DuvR9570fAvMqdO8U/UVumN0lfwIekV+Cpd5mGxDdxo1Tjgl/dlwAAAABJRU5ErkJggg=='}],
                               ['xul:menuitem', {label: 'YoukuAntiADs.uc.js',oncommand: 'UpdateYoukuAntiADs();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '广告视频播放器更新',oncommand: 'UpdateYoukuAntiADsSWF();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'ButtonEventListener.uc.js',oncommand: 'UpdateButtonEventListener();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'QuickOpen.uc.js',oncommand: 'UpdateQuickOpen();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'QuickSnapshot.uc.js',oncommand: 'UpdateQuickSnapshot();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'QuickSwitch.uc.js',oncommand: 'UpdateQuickSwitch();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'QuickTranslate.uc.js',oncommand: 'UpdateQuickTranslate();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'SidebarMod.uc.js',oncommand: 'UpdateSidebarMod();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'SimpleMusicPlayer.uc.js',oncommand: 'UpdateSimpleMusicPlayer();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: 'TalkwithWallace.uc.js',oncommand: 'UpdateTalkwithWallace();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '_addmenu.js',oncommand: 'UpdateAddmenu();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '_keychanger.js',oncommand: 'UpdateKeychanger();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '_redirector.js',oncommand: 'UpdateRedirector();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ['xul:menuitem', {label: '_userAgentChange.js',oncommand: 'UpdateUserAgentChange();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA0UlEQVQ4jZXOMSuFYRjG8R8jkkIxKasBi/g2FsmmlEXqKBkMMirjKQxsjAaLb+IYfIjXcqm345znec+9PN3P/f9fXZTnGg2ecFxh/81NZFjD+yRyryXDbPaVLvJp4JnWX4OHLvJh4NUh+bmLfBD4ZEi+z7tVC2jwgSPMZb/NbT/7ZingJW8/8MWYhhulBpeYxvkY5i9kZ9RxKsezUs0wVzVgr9KyOAsBt0fIdzUZlvAWYbkl97vI8IN57OI18mNXGQaR4Qufk8iwju8EDbBYgn8B5PE1NODs8/QAAAAASUVORK5CYII='}],
                               ]
                               ],
                                ['xul:menuitem', {label: '配置文件夹',tooltiptext: 'Profiles', oncommand: 'var canvas = Components.classes["@mozilla.org/file/directory_service;1"].	getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsILocalFile).launch();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAbUlEQVQ4je2Syw2AIBAFJ1ZjD1RgJbRhtVSBWS/rj2UjqEcn4fKSN7wQ4CMmIAFyOknzJhIQiixo3iwYK7k4x6zzBB5m3Wb1bqwtkFJQvsHdAiPoRQCGB8ULv+AQLEDs6EXt7MxApv0jZe28ZwV+VzP4VojXiwAAAABJRU5ErkJggg=='}],
                                ['xul:menuitem', {label: '脚本文件夹',tooltiptext: 'Chrome', oncommand: 'var canvas = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("UChrm", Components.interfaces.nsILocalFile).reveal();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAbUlEQVQ4je2Syw2AIBAFJ1ZjD1RgJbRhtVSBWS/rj2UjqEcn4fKSN7wQ4CMmIAFyOknzJhIQiixo3iwYK7k4x6zzBB5m3Wb1bqwtkFJQvsHdAiPoRQCGB8ULv+AQLEDs6EXt7MxApv0jZe28ZwV+VzP4VojXiwAAAABJRU5ErkJggg=='}],
                                //['xul:menuitem', {label: '图片文件夹',tooltiptext: 'Images', oncommand: 'QuickOpenImages();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAbUlEQVQ4je2Syw2AIBAFJ1ZjD1RgJbRhtVSBWS/rj2UjqEcn4fKSN7wQ4CMmIAFyOknzJhIQiixo3iwYK7k4x6zzBB5m3Wb1bqwtkFJQvsHdAiPoRQCGB8ULv+AQLEDs6EXt7MxApv0jZe28ZwV+VzP4VojXiwAAAABJRU5ErkJggg=='}],
                                //['xul:menuitem', {label: '工具文件夹',tooltiptext: 'Software', oncommand: 'QuickOpenSoftware();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAbUlEQVQ4je2Syw2AIBAFJ1ZjD1RgJbRhtVSBWS/rj2UjqEcn4fKSN7wQ4CMmIAFyOknzJhIQiixo3iwYK7k4x6zzBB5m3Wb1bqwtkFJQvsHdAiPoRQCGB8ULv+AQLEDs6EXt7MxApv0jZe28ZwV+VzP4VojXiwAAAABJRU5ErkJggg=='}],     
                               ['xul:menuitem', {label: '火狐根目录', tooltiptext: 'Firefox', oncommand: 'QuickOpenApplication();',class:'menuitem-iconic', image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAbUlEQVQ4je2Syw2AIBAFJ1ZjD1RgJbRhtVSBWS/rj2UjqEcn4fKSN7wQ4CMmIAFyOknzJhIQiixo3iwYK7k4x6zzBB5m3Wb1bqwtkFJQvsHdAiPoRQCGB8ULv+AQLEDs6EXt7MxApv0jZe28ZwV+VzP4VojXiwAAAABJRU5ErkJggg=='}],     
                        ];
        aNode.appendChild(jsonToDOM(myMenuJson, aNode.ownerDocument, {}));
        aNode.setAttribute('menupopup', 'QuickOpen_pop');
    }
});


//定义图标
var cssStr = '@-moz-document url("chrome://browser/content/browser.xul"){'
		 + '#QuickOpen[cui-areatype="toolbar"] .toolbarbutton-icon {'
		 + 'list-style-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA2UlEQVQ4jbWRIRKDMBREn8NhOoPrDLoqtifgAnG9AUfgAJU9SS2uAoOt6gWqIiqjqlrBMhNChoLozjA/2ewu/yfwRxRAC3jVYmtAD5yBXLXfYj4BndZ71U78KtwAK9NLf7fiV8HLMLY9Bvg1ZgM8gSoKqMSbJXMFOOACZDKOI2TinXQzWB0eI76M9kfpbBzgf7UXwJC4j0+w3iXCjPiUfkI0DHM7htlbhud04pulgLsMJXBQmw/VUl8r3SzgDdTBPpdxDMqDs1r6CeLbh+HZvOoa/QwZcFVN4gv6nzt18jn5zAAAAABJRU5ErkJggg==)'
		 + '}}'
     + '#QuickOpen[cui-areatype="menu-panel"] .toolbarbutton-icon, toolbarpaletteitem[place="palette"]> #QuickOpen .toolbarbutton-icon {'
		 + 'list-style-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACOUlEQVRYhe2XIW/jQBCFVyowC1grnu9JhQUlRSHB/QWFJYUlpQUlBYUlYaGGJZFCgkvCwkMKQ8JMIoXfkfFpa7lq0tgnnXQjrWTvrue93Z15Ow7hX7QY4wCYSqqALTCNMQ7+Fn4GrCS9mVlhZoWkN2AVQsh6RwdegVlL/wyY9go+HA4vgK2ZFc0xMyuA7XA4vOiNgJ/781fjkp773IVMUtVYfRaSc/d4qEIfsWBm18CyfgdegB2wB16S/qWZXXdOAJgATyGEIOkeWNdZAKwl3fu8J2DSB4E/K5P0YWbjeszMxpI+/PnTTnVJYBdjHCTn/Mnq+HCR2nUKnjr1VNy0ENzUKViT7YyApBGwTsjsw+dIz4B9DeoxMeoEPM/zS2CT5jfwLukuIXgHvCfjJbDJ8/zyJHAzuwK2KZj3jyVVku68VWlQ+pxbV82rH4HHGM/dwe0X5MbAAlg0wZskYoznx+JnwBp4OPbDpgEPHj+Hq6OLTnkqeOKvPEqcWjT/JHO1PFwbJP3qCvxHPv8TaJvs51gCszaZdYWcAWVb/JxEoCiKG6+AX53E1jPlwdvE+0rgVVJVFMVNJwS88Nik2u4CNG20Mvl+5PL9cgqBzMvtZXNLgVUqsa6Kq3SOH9nSiWVHEwDegXloUTBgnkq03wdvLa4yYO6X19EEyhDC2Rfj98AiIbRoXliJnQHlUQS+K61jjANJVZ7nl35db8M3Wt95ue7F59zbY6fOD7RM0ocXo/3/E7YZ8Hjq6n8Drom0kObZsBkAAAAASUVORK5CYII=)'
		 + '}}';
	var sss = Cc["@mozilla.org/content/style-sheet-service;1"].getService(Ci.nsIStyleSheetService);
	var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
	sss.loadAndRegisterSheet(ios.newURI("data:text/css;base64," + btoa(cssStr), null, null), sss.USER_SHEET);




//定义函数

function	QuickOpenImages() {var file = Services.dirsvc.get('ProfD', Ci.nsILocalFile); file.appendRelativePath("extensions\\userChromeJS_Mix@develop.com\\content\\images"); file.launch();};

function	QuickOpenSoftware() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\"); file.launch();};

function	QuickOpenApplication() { var path ="..\\..\\..\\..\\";	var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);file.initWithPath(path.replace(/^\./, Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile).path));file.launch();};

function	QuickOpenUserjs() { var file = Services.dirsvc.get('ProfD', Ci.nsILocalFile); file.appendRelativePath("user.js"); file.launch();};

function	EverythingSearch() { var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\Everything.exe"); file.launch();};

function	Setnotepad2() {  var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\NotePad2.bat"); file.launch();};

function	SetIDM() {	var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile); file.appendRelativePath("Local\\IDM\\!绿化卸载.bat"); file.launch(); return file;};




 function QuickOpenMyComputer(event) {
			try {
				var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("WinD", Components.interfaces.nsILocalFile);
				file.append("explorer.exe");
				var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
				process.init(file);
				process.run(false, [","], 1);

			} catch (ex) {
				alert("打开我的电脑失败!")
			}
};


 function QuickOpenVolume(event) {
			try {
				var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile);
				file.append(/6/.test(navigator.oscpu) ? "sndvol.exe" : "sndvol32.exe");
				var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
				process.init(file);
				process.run(false, ["-f"], 1);
			} catch (ex) {
				alert("打开音量控制器失败!")
			}
};


 function QuickOpenTaskMGR(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\taskmgr.exe");
				file.launch();
			} catch (ex) {
				alert("打开任务管理器失败!")
			}
};


 function QuickOpenIE(event) {
	try {
		var file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("ProgF", Components.interfaces.nsILocalFile);
		file.append("Internet Explorer");
		file.append("iexplore.exe");
		var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
		process.init(file);
		process.run(false, ["about:Tabs"], 1);
	} catch (ex) {
		alert("打开IE失败!")
	}
}; 


 function QuickOpenNotedpad(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\notepad.exe");
				file.launch();
			} catch (ex) {
				alert("打开记事本失败!")
			}
};


 function QuickOpenInetcpl(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\inetcpl.cpl");
				file.launch();
			} catch (ex) {
				alert("打开Internet选项失败!")
			}
};

 function QuickOpenCALC(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\calc.exe");
				file.launch();
			} catch (ex) {
				alert("打开计算器失败!")
			}
};

 function QuickOpenCMD(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\cmd.exe");
				file.launch();
			} catch (ex) {
				alert("打开命令行失败!")
			}
};

 function QuickOpenOSK(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\osk.exe");
				file.launch();
			} catch (ex) {
				alert("打开屏幕键盘失败!")
			}
};

 function QuickOpenMagnify(event) {
			try {
				var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
				file.initWithPath(Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("SysD", Components.interfaces.nsILocalFile).path + "\\magnify.exe");
				file.launch();
			} catch (ex) {
				alert("打开放大镜失败!")
			}
};

function UpdateYoukuAntiADs(event) {
//更新视频广告过滤脚本
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/YoukuAntiADs.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("YoukuAntiADs.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本YoukuAntiADs.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };



function UpdateYoukuAntiADsSWF(event) {
//更新视频广告过滤播放器
	var file = Services.dirsvc.get('UChrm', Ci.nsILocalFile);
	file.appendRelativePath("Local\\UpdateYoukuAntiADs.bat");
	file.launch(); return file;
    };




function UpdateButtonEventListener(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/ButtonEventListener.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("ButtonEventListener.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本ButtonEventListener.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };



function UpdateQuickOpen(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/QuickOpen.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("QuickOpen.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本QuickOpen.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };




function UpdateQuickSnapshot(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/QuickSnapshot.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("QuickSnapshot.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本QuickSnapshot.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };




function UpdateQuickSwitch(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/QuickSwitch.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("QuickSwitch.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本QuickSwitch.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };






function UpdateQuickTranslate(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/QuickTranslate.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("QuickTranslate.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本QuickTranslate.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };






function UpdateSidebarMod(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/SidebarMod.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("SidebarMod.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本SidebarMod.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };



function UpdateSimpleMusicPlayer(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/SimpleMusicPlayer.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("SimpleMusicPlayer.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本SimpleMusicPlayer.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };





function UpdateTalkwithWallace(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/TalkwithWallace.uc.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("SubScript");
        target.append("TalkwithWallace.uc.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本TalkwithWallace.uc.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };






function UpdateAddmenu(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/_addmenu.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("Local");
        target.append("_addmenu.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本_addmenu.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };




function UpdateKeychanger(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/_keychanger.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("Local");
        target.append("_keychanger.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本_keychanger.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };





function UpdateRedirector(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/_redirector.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("Local");
        target.append("_redirector.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本_redirector.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };




function UpdateUserAgentChange(event) {
        var url = 'http://git.oschina.net/runningcheese/firefox/raw/master/_userAgentChange.js';
        var uri = Services.io.newURI(url, null, null);

        var target = Components.classes["@mozilla.org/file/directory_service;1"]
                .getService(Components.interfaces.nsIProperties)
                .get("ProfD", Components.interfaces.nsIFile);
        target.append("chrome");
        target.append("Local");
        target.append("_userAgentChange.js");

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = persist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
        persist.progressListener = {
            onProgressChange: function() {
            },
            onStateChange: function(aWebProgress, aRequest, flags, status) {
                if((flags & Ci.nsIWebProgressListener.STATE_STOP) && status == 0) {
                    if (userChromejs.save) {
                        userChromejs.save.showInstallMessage('脚本_userAgentChange.js', '已成功更新');
                    }
                }
            }
        };
        persist.saveURI(uri, null, null, null, null, null, target, null);
    };







