DISPLAY_TYPE =1; // 0显示列表为radiobox, 1显示为ua图标列表

SITE_LIST =[

	{url : /^http:\/\/vod\.kankan\.com\//, Name : "Safari"}, //直接可以看kankan视频，无需高清组件
	{url : /^http:\/\/v\.huya\.com\/play\//, Name : "iPad2"},//虎牙网页版
	{url : /^http:\/\/www\.meipai\.com\/media\/\d+/, Name : "iPad2"},//美拍网页版
	{url : /^http:\/\/i\.jandan\.net\//, Name : "iPhone"},//煎蛋网页版
	{url : /^http:\/\/m\.neihanshequ\.com\//, Name : "iPhone"},//内涵段子网页版
	{url : /^http:\/\/m\.juzimi\.com\//, Name : "iPhone"},//句子迷网页版
	{url : /^http:\/\/wap\./, Name : "UCBrowser"}, //WAP用UC浏览器
	{url : /^http:\/\/m\.taobao\.com\//, Name : "iPhone"},//淘宝手机版
	{url : /^http:\/\/m\.toutiao\.com\//, Name : "iPhone"},//头条新闻手机版
 {url : /^http:\/\/sports\.qq\.com\//, Name : "Nokia"},//腾讯体育去广告
 {url : /^http:\/\/sport\.qq\.com\//, Name : "Nokia"},//腾讯体育去广告
 {url : /^http:\/\/kbs\.sports\.qq\.com\//, Name : "Nokia"},//腾讯体育去广告

],

UA_LIST=[
{name: "分隔线",},

    //伪装 IE8 - Windows7
    {  name: "IE8",//此处文字显示在右键菜单上，中文字符请转换成javascript编码，否则乱码(推荐http://rishida.net/tools/conversion/)
    ua: "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)",
    label: "IE8",//此处文字显示在状态栏上，如果你设置状态栏不显示图标
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB40lEQVQ4jY1TPWtUURCdGIstjLIfd+bZWIuFoAiiWAiiELCwUCsJ2yiCKIIkxm7T5Gtn7hps/AEBdbXQQhvBVwjix9s789KJWghBiUXQQowWeRa6a95uEvfCqWbOYc6ZuQD9vtqT7RVpnUSvo8g65qbe7Nu4OcsGSvVwyHEYJ7E58nYPRVfI2xJJuOU4XEaxByV+uaeHW64nB9FbiBpp1g304Qd5m4JqXICjta0oYSRHdvXWWRL7RaKr6wl0hMSeQy3ehqynOuTKbLIfxb6g6Dx6/Rk10oy8fSWvT9ebiNjiykw40BFA0XnkcBFFV6JGmjmvj4vTyY52vcR6HMW+5yaZTU4AAMDQ5Kuy4zDsRJejRpqR2Le15I5FsetrBZwP99uxbyHWF53xvH4g0au9sLmcDR8+AwAAiZ7fLLSNQKKrANW4QN4+5QvhveMw/j+g11FACSO9+7ZFaDYH+7hPAJLwiLw9Q9GPORHWsVxjszlIYndzIYpeAvK2FN1cOIPSOtfj0evDnbxwwUm44thed9XeQTUuQNRIs+J0sguybADFbvcTnhNdLtZbe/8ckLdFx8mR9jqRbWLTM2bT8mSy+9/nkXDsbwYzTvQGcrhWEj2MbBPEFqO3t8QhJW93HIfT3eH+BsP2rrdUExzOAAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    //伪装 Apple iPad 2
    {  name: "iPad2",
    ua: "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
    label : "Apple iPad 2",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABUElEQVQ4jY2SPU7DQBCFv50dZCWKZUMKJGxR5AqurG22SMExSBdRchO4AAU/QqKhpQAZhYICDgMcYCmwiWNwkpGetNrV+3Y0b2BZY+ASeN1CFXBEp86AD+AauAKuROSmUXNX6x34BLQNuK3JAKYsy0FRFMOyLAeA6Xx2DAQg/g8gzrl4Oi0S51wMSLfVBpCmafoHsIUZYCYiIUmS3RWAqi62MEsURXNr7UoHxlp7p6ovm8zOuXg4jE5EZDkD773WgMUm83RaJFEUzfkZ4giAoih2rLV3LFPoNdcdzlYA3ntlNcZ1ZmEZ4+h3Bj2Avkj/APgHsG4fegFvwC6wl2VZPpkcHGZZlgN7QNrSKT2bGICgaoOqBlUbgCfgq3lrq7uJ50AQkWCtDXXOAdgH7rtmY8xHnueDNmAsIhfGmGdjeK7nUQGPrXMFVMaYB1U98t7rN9evbHQyTKAEAAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    // 伪装 Nokia E72
    { name: "Nokia",
    ua: "Mozilla/5.0 (SymbianOS/9.3; Series60/3.2 NokiaE72-1/021.021; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.1.16352",
    label: "Nokia",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACgUlEQVQ4jY2T30tTYRjHX2G76M8Io6tNhYGwKw9H+6GE01JQvJBIj7j3nIuMSsroImUSGURXs1+UhEIu9Wx7z1D8UUt0c06XTDfPOesH3liilnoRKt8umofcMnzguXnf7+fhed/n+RKSEVeG4qcoUztFps9QRf+WzrDo113CwEJupt4IIRIxU0W7TxVtXwqkIAVSEJlqpBRIQVS0PdGvu4RIxJwFO5kuH4CSokP0JUG9CYi+JCRFh3EXSIEq2mC6SA4hhJBzrQ+Vhr4ZA6byIuhQHFRezIKlQApC/xxKbz2SOY4zkTMtXcXWkkrkna1GRfszOOX4f+GqB30oKKtBXkkFzt/oKiZFjbenLLwDFt4BK1+OsssSrg7NZ8O+JVyU7hg6K1+Ooqa2KVJY1bT992Ft8zV8TS1j6p2CT7OjmI2G0DsZQ/LLCi7UOw2dlS9HYbWwRfLLavYPCuQVO/A+FMV62IOteRk7c17EI+P4lRjHjh6Ch40YsIV3oKC0dp/UdQ9v8M13kV9SCbGtA3FVxcZkD9aDr7AefIm1Dz3Y+ahgcz6ApLqMOnodFt4Be52IS529PwhV9LDIVAhvomiRY7g3nsLPRBDbMS/WJl7g+9hTrIx0o390Am3DS7jpCaP++difcTJ1moh+3ZX50wOTUWzGg9gIe/B5+Ale+xikf0yFMq2DCAMLuaKi7WUWeTy2gFZvDOIRI6VM322QEycJISS7i2Msk5Np7cYmpn0weFyYKtpbwe02cxxnOuQH0a+7nPLS7pEeYPquk2ntgtttttvtJ2w22yFT5XAcZ2rsC52mvkQnZeq0yPRVkemrlKnTlGkd6TfncBxnstlsZo7jTL8BxjBnKrVDKTMAAAAASUVORK5CYII="},
    {name: "分隔线",},

    //伪装 iPhone，查询http://www.zytrax.com/tech/web/mobile_ids.html
    {  name: "iPhone",
    ua: "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_2 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7D11 Safari/528.16",
    label: "iPhone",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABG0lEQVQ4jaXTPUvDUBSA4f40g3Uo+LU46CJ0Egr+AHGuLdRGECqog8VJ6x4H3f2gqdG0RLBiagaDqVANTWxSXxctCuaa6oEzXLj34ZzDuYmRbJX/ZCLOpVShxsz65d+ATNmg0w0oKubwgJSt0nI82m6PCVmLD8yWdOY2dJJ5lf1Tm8XdazJlg1ShJgbGixec33b4jKbdpW65g7PrhyxVbqIBRXMQhd/rs7Bj/AyM5lReg74Q2Dt5iG5hek0TPgaQD1vRQDKnEvbfhMCR3hYPUf8ysKgoHVtIKxHAqmL+Ctw9ekhRFYzlVUzHEwLLB03xIqW3Gzx7IQDWk8/V/QvBx2wqZ3a8TZySNdJbjUGpk7LG/Gb9W+lD/UZRvgOrbAPuPPtl+wAAAABJRU5ErkJggg=="},
    {name: "分隔线",},

    //伪装 Opera 10.60
    {  name: "Opera",
    ua: "Opera/9.80 (Windows NT 6.1; U) Presto/2.6.30 Version/10.60",
    label: "Opera",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABVUlEQVQ4jaWTzU7CQBDHeQjxJH4lkvjxChpINAENiAkPoYFE8IRPIB64iAd4CE6o3GqUA0lPPemFC3YprIhREtuttP17aCyY5aPETSbZ7Ex+szPzH8+Ldw7/Mc84R3NrEzQaAY1G0NxYdw9o7+2C1WqAaYKJIpgo2vdqFa1gYDLgNR6HpesAgLeTY+e9m0wCACzGQGOHowFkdQVGpwMA6DcafzPNe2EQAgAwFAXyoo8HvJ9n8Hu+SiXuq2q57Pi7Z2keoN0LTkDvOs8BeoWC41crFR7Qr9edgI/LLAf4zOUc//fz0xRA9mJ2gCYMlZC/4ksoFgcl3N2OaGJmShNvhpqYOuUB8vISDErHj1FR7DESAtm3MFpI9CgGizE7SyIxEFI6ZQtJVUEP9idLuRUMQHt8AEwTuiRBlyTAMKAJAlo72+6Xifj9aIfDaIdCIP612bfRrf0AUqaXFMVw1DwAAAAASUVORK5CYII="},
    {name: "分隔线",},

    //伪装 Safari - Mac OS X
   {  name: "Safari",
    ua: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10;  rv:48.0) Gecko/20100101 Firefox/48.0",
    label: "Safari",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACAUlEQVQ4jXWSQWgTURCGh0Js3hstxbtHjxLEe49ePIggUnqQIIInT14VgohrbdJgY0mEBHeeN5HWQ5ujW0S8qG9GQnaTGAOCePHY0iZIdj2kSdpk8992//lnvnkMwIQS5S+XNUlJudJC4h4S95SRpjZSTFRsarJ+rPW3Shspn33zPVLE+5pkS5E4isTRhre1kQMkDjVJCTJeciqsXPsRDfcViQOrn85NDdioLiDxGhKHyvDeqSbaSBkN95X77dYswB/rzlUAAGV4BYlDbaQ42nmALc6scHs1U2hnnzwcfiNJDonDRMWmQJOUFPF+LDYAtHJP73XuLv/z0ukxct5bRJJDRbwJypWWJtmKC/vZZ0ud9M1+59EDmfS0KzuKbABI3IvDj6Jorv388d9fy9ein3nnzqSPrmTRcHdmA9/3lxrNZtR8/64ZAczNbDA4Et6eLAiC4FXQaPyp1WoX4tZDw7vasA/aSFEbOYCN6sLQrFar834Q/A6C4EpcGMqfz6OxR5psARIVm0LiEInXhn69Xr/u+/6N2DAAoJEXSByeef31EgAAaJLS8YWtAAAw88VZ4aRrbw8OiV+O/2a8pDK8NyCRHOS9xXjswWQk+QD3q/OnCzJeckiCJIfalR10JXv82rto7NFo8lT4hBIVm1LEm4psgIa7aLirDfuabGG08wn9B7eqKcVfaInpAAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    // 伪装 Chrome56.0.1219.0 - Windows7
    {  name: "Chrome",
    ua: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
    label: "Chrome",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACH0lEQVQ4jY2SvW/TQBjGXzq0UAWpA6k7IYZsmYqT3BkGrLbO+dyu+SsQI4yoKxI2qtRUpRFEsBQ5TsYiFfElloqP3GXo0ipIDFXtglASEBKxm2NI49oJRTzSs93ze78OYEhfEZIPc7lHHkJNF+Oui5DvIdR0Edo4yGRmh9+H2k2nEx5CT44URZxlF+Oei9D6Vio1EQt/lOVJD+Odf4Wj9jB+IQDGQ8B3Wb7sYfzrfwFHiiIOEboPAACard0g9rzuIXQ3bBWhVwfZ7LVYlbNEquQpdbRdu5BOeAg1PYy3CwU7kbRYUbJ4S7JYJ2mx4nJ5ecrn9H2vsSgGDphRBN0hn42aLoij3XRzuaUv2Ww6abHizIOGiHraZKv+B+16FOBzugfUIb5R04XukG9qWZ0CAJAs3hoGSCZvCwHnAkZ/nHZAf4eAE4jZB7DOCMDiLQCAgNFODDAYwajpglZJV7XV1N9GkEy+0q1rmfgO6D7QKnk8APQh+ZeX7ty7OG2yVcnkbcniLcnkK3BrayLgxvMhwAYQey4XBRg1XdCK9oY8m7t6cqgxABgLPuWXouFeY1F061oGAAB0h5SHIf2d5Du0SraFXRj3Od2LVa8bpfAvyA/lSVrR3o104pCetqlljpl+O3a+uvFWvL5yPvahFEu5oFdJiTqkF7lK+efOvBRwox2tPBKOamFzYZY6+TVSyTPVVmcCbqwHjO4HdaMUzhzRH3mb3suTHrIEAAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    // 伪装 Android Droid
    { name: "Android",
    ua: "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
    label: "Android",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABMklEQVQ4jbWTwUoCURSGfaWaQKjoGcLsAXJhkOAiCNoWtWvhwkmsjYucXZpiRYu5owY6Cx21gpEQJje6KDICadV8bRohxomR6MAP954L37mH859AUp3jLwpMS55Ulkmq85O7LKTvnE9ASgtS6yUw+hmMfoZaL0FKC/oHdIeXPL1UccJ6rmIOCv4AeSMKwPvHYAJwzrlmxBtwLBY4b2yg6GFuH4+mKlsPkWtGkIXkBhTbMQCEuY9XOG8XxqYbUOrEASh3Dz0BWvcAgGI79g+AQmsLsH9tQTX3AMgbUTdAFhKKHubmYdcTcH2/g6Kv/zCZa4yKvsbwrQ3A69hiNLYAGIxanNVX/RnptLIyqej8KF1e8u/EdHkRsCl14lzdbWPbn7NZ2WlFFhKykMjqodm2cRZ9AdmG7E2iBcE/AAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    //伪装 微信
    { name: "Wechat",
    ua: "Mozilla/5.0 (Linux; U; Android 5.1.1; zh-cn; NX510J Build/LMY47V) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/5.4 TBS/025489 Mobile Safari/533.1 V1_AND_SQ_6.2.3_336_YYB_D QQ/6.2.3.2700 NetType/WIFI WebP/0.3.0 Pixel/1080",
    label: "DoCoMo",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABCElEQVQ4ja2TsU4CQRCGJ4RH4Bkol52JMZYXSsPdzfhAhoLwFIbiCl5js//2xsJYWFBZURIKcyHRAhWRiwLnJH+zm+/PP7szRP9aY+pYGowUXCl4YUlqS1IreKHgytJgRGPqNLIFxCnk3pK8/S5+yOFkD86jGyp4/Te8lYLXeXTDLRz6PQMvj4W/BF7mod8jBU+/X3ym+gk0nSt4SgZ+bmHwRAbZnBx/Z/BKZ/W/+5EXUvC8RYI5KQaXZ7UB2eTBXRERURn95FSDMvrJ3jBp4rsj4drAtwejbPCzj4d51CSrg36TrBRcFRDXuAsGP9PgMyKiLFC3CHxhUW7K6K81eJ8F6jZvYMt6B+ijiYfyJmTMAAAAAElFTkSuQmCC"},
    {name: "分隔线",},

    //伪装 Google 爬虫
    { name: "Googlebot",
    ua: "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
    label:"Googlebot",
    img:       "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACHklEQVQ4jWNgwALu9/cLfJjQXvqhtXbl+9aatR96mzteT+hUx6YWBdzoLOP9UFe276Wvw79Xzqb/UbCL+f+3hen33k/tMsCq+dXUPpW3yRGfMTSi4ddR/j/+z5zJiqL5dm4u+9vkyI/ICt8khn59V1N89ENj5a53+SlPX7ma/3/l7/Tv87QJoRi2f55fuRyu2dX8/4fOhnn/GRgYkdV8nNQT/WFqbziG5v//GRh/HpL//XOrx/9XHub/3zVXbiUYWMjgx7Vi1997WP//3sP6/8dBy9/vZs7kR5bv2vhtSvvGb8uw4WlbPigx/L6WWQMz4NdJizfoFoRP+vLLqfXLf2x4wvZveQy/r2VVwwz4ecLiLSkGTNz2rYzh+80yZ5gBJ4/6/G44MYkP2YDWdd/W1q3+dgCGvbs+/4MZMHPn1wCG//8ZGH8cUvq14HDqf7OVIf8rj/biDMT+bV8DYJq9uj7/a5j/n4OBgYGBYfqJnmUmK4L/m6wI/m+2MvR//YlJCxn+o0bjhL2n7KKmfPkBM6Bs2dfLcMncbbnsUduLPsAMMVkR/D9wS/a3gv1tJ0oOd+xP2lX52HJV+P/07TP+u3a8/+/b8/nf1F1fUJP0tEtLlcK3F35CNgQbjtlW92fCjs85WP1YsrObu/Jo907HtbF/0TWarQz5n7S7+ln/6fkOuMIIxaDO03Ny6k9MWlh/fOLK1lMz2npOzNPCphYAFk+bEHlmFVMAAAAASUVORK5CYII="},
    {name: "分隔线",},

    //伪装 UC 浏览器
    { name: "UcBrowser",
    ua: "NOKIA5700/ UCWEB7.0.2.37/28/999",
    label:"UcBrowser",
    img:  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC8klEQVQ4jW2T3U9TdxzGH9yqibvZHf4Hy6574eWMV/WG3SwmTUABTctKbcqxB9qxuonz5U4svqwXRrcY6kvcPFsobyec9vTt9MV1gGYoUmk8YAckpzEB4w7yeHFgEOIn+dz9nie/b/L9AjtxRW3wKQ50pWI4ndUhZk2IWROnszq6UjH4FAdcURs+ykm5EUI6AjFrIFQgeovE96VtQwVCzBoQ0hGclBs/FpbQoxG9Re49X+ZnF/8mwpvh8Ka9RaInTwhpabvEHrXBr0bQrXHvuTKDowssLaxy+t819imv+WnfX1Zwp90a4VcjsEdtgFt2QMgYCBV48Od/eKu0wtV36yRJ8/0GnffniGCB+38qsyFcIkJFaxwhY8AtO4DORAyiRgQLRKjAPeES5dk6LTbo+7NKBDSeGV9gy4MK8V3ReitqRGciBniTOkTNmk3M88vL03yx8pbkBp8tr9F+9SmPDr5g1XjL+ppJjzRPdOetAm9SB7xJEwGNCGj8/OxjjswYNNffM1Wps/1Bhd/cmeXVdI3jzwzW1/6jVn3DfWdKREAjvEkT8CRMCDlCyLFnqMp35jrvlZfZ8bDC87LOK+prXs/UeLe8zH51kb5HL7knmCeEHOFJmIB7Qoc/QwRyfDS1wtmlVX517Qn7Rl/xkqyzP7nAAXWRN3M13ikt8eubM2wQNcKfIdwTOnBCjsGbIvwZ3tZqTM8ZbB18zmvqIi8rOs8OV/nb5DIP/Fhkg5AlfGnClyG8KeKEHAOahxxwKQY6Uzw0MMX8yzrDQ/P8NV/jL1qNP8TnmZw1+MWFx1ZoS5dioHnIYS1S63AE7gTRqfLQlUl23HvOIzemeXhgisE/KnxYXmLg9znaAhnCoxIdCaJ1eHORAMApNaJtRIJL4VYRPCrxbZKf+FNsOGUJT5JwKUTbiASntOsemgYbcSweQbts/F+0U5dCtMsGjsUjaBrcFd7CHrXBKTnQEo/h+KiOtjETbWMmjo/qaInH4JQc29+2+AC0Gi3ocvFI9AAAAABJRU5ErkJggg=="},
    {name: "分隔线",},

    //伪装 firefox10.0 - Windows7
    {  name: "firefox10.0",
    ua: "Mozilla/5.0 (Windows NT 6.1; rv:10.0.6) Gecko/20120716 Firefox/10.0.6",
    label: "firefox10.0",
    img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB4ElEQVQ4jY1Sv2sUURCeffvj1qy57ImIBAWRICISgvUhQUKwsBArSREklYWIhX/AWojFvZl3d3tHsMgfcEWqlEFSiKQIFhbB6rCwFLncvhlCEDmLaLxb34ED08zM982PbwAc9i2fmS+6tbrtpHeLbq1etGr1IVUvuGon7Hv73BWbz907MnPXx+N72XLA+cU16aQbA5Om/wAthc+4naxys7YIAMDNaLHozN4Yr5H86jxTtCMI/UIH9QkCQW+fNXxmUj0hf5vJ+5hloMqNCh3dFPTeC4LlvLr2l4BUXxBGY95nCu7bRnS7TPIlg5i190k0HA9MmgKbeKUEPnUNJ5bCp65bSXNmUxBGbOIVYPS3XASFiR5OOzZreCcII24lb4Ap7JXBbIKXQ6osuMADk6ZC3oEgjIT8bWDyd8sE1kSPAAD2MgjKkrKpvBANJ4IwYlQtsBS/cuz/U9D7IBq+CoJwM1wHAMgyUIyqcTYphuvAJllyHpGCA0a1dWTiswm4PfuEtXf4u8kP7iaXTxOONVh7h5aix3bz/KUhVReE/LcTeVK90oOAOCdxOCMMBya+NqktVh6IhuP/IBCmYNWpr5jwjqC3P7Uz+btWR7em/ceYVMmSRfWcUTUsqdeC/kZZzj/2C7Xqd/NLI9L5AAAAAElFTkSuQmCC" },
    {name: "分隔线",},
]
