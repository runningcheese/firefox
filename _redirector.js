rules = [
/*{
// 示例
name: "about:haoutil", // 规则名称
from: "about:haoutil", // 需要重定向的地址
to: "https://haoutil.googlecode.com", // 目标地址
wildcard: false, // 可选，true 表示 from 是通配符
regex: false, // 可选，true 表示 from 是正则表达式
resp: false, // 可选，true 表示替换 response body
state: false, // 可选，表示该规则默认关闭，state: true,  表示该规则默认开启，需要时能点击使用，重启firefox后恢复关闭状态。
},
 {
name: "google链接加密",
from: /^http:\/\/(([^\.]+\.)?google\..+)/i,
exclude: /google\.cn/i, // 可选，排除例外规则
to: "https://$1",
regex: true
}, */
{
name: "Google中文 >> Google英文",
from: /^https?:\/\/www\.google\.com\.hk\/search\?(.*)/i,
to: "https://www.google.com/ncr#$1&hl=en-US&safe=off",
exclude: /^https:\/\/www\.google\.com\/.*\&hl=en-US&safe=off(.*)/i,
regex: true,
state: false, 
},{
name: "Google搜索 >> 关闭安全搜索",
from: /^https?:\/\/www\.google\.com\/(s\?|search\?|webhp\?)(.*)/i,
to: "https://www.google.com/$1$2&hl=en-US&safe=off",
exclude: /^https:\/\/www\.google\.com\/.*\&hl=en-US&safe=off(.*)/i,
regex: true,
},{
    name: "Google快照 http >> https",
    from: /^http:\/\/(.*?googleusercontent\.com\/.*)$/i,
    to: "https://$1",
    regex: true
},
/*{
    name: "Google Redirect Notice",
    from: /^https?:\/\/www\.google\.com\.hk\/url\?sa=p&hl=zh-CN&pref=hkredirect&pval=yes&q=https?:\/\/www\.google\.com\.hk\/search\?(.*)/i,
    to: "https://www.google.com/ncr#$1&hl=en-US&safe=off",
    exclude: /^https:\/\/www\.google\.com\/.*\&hl=en-US&safe=off(.*)/i,
    regex: true
    },*/

{
name: "去除Google搜索验证",
from: /^https?:\/\/ipv4\.google\.com\/sorry\/index\?continue=https?:\/\/www\.google\.com(?:\.hk|)\/search\?(.*q=.*)&q=.*/i,
to: "https://www.google.com/ncr#$1",
decode: true,
regex: true
},{
    name: "去除Google搜索结果跳转",
    from: /^https?:\/\/www\.google\.com\.hk\/url\?sa=p&hl=zh-CN&pref=hkredirect&pval=yes&q=https?:\/\/www\.google\.com\.hk\/search\?(.*)/i,
    to: "https://www.google.com/ncr#$1&hl=en-US&safe=off",
    exclude: /^https:\/\/www\.google\.com\/.*\&hl=en-US&safe=off(.*)/i,
    regex: true
},{
name: "Userscripts >> Mirror",
from: /^https?:\/\/userscripts\.org\/(.*)/i,
to: "http:\/\/userscripts-mirror.org/$1",
regex: true
},{
    name: "Wiki繁体 >> 简体",
    from: /^(https?:\/\/zh\.wikipedia\.org)\/(wiki|zh|zh((?!\-cn)[^\/])+)\/(.*)/i,
    to: "$1/zh-cn/$4",
    regex: true,
},{
name: "WiKi http >> https",
from: /^http:\/\/([^\/]+\.wikipedia\.org\/.+)/i,
to: "https://$1",
regex: true
},
{
    name: "Google HK http >> https",
    from: /^http:\/\/www\.google\.com\.hk\/(.*)/i,
    to: "https://www.google.com.hk/$1",
    regex: true
},
{
    name: "常用hosts网站1 http >> https",
    from: /^http:\/\/(.*)?(youtube|google|wordpress|github|twitter|deviantart|facebook|pinterest|vimeo|feedly|flickr|quora|instagram)\.com\/(.*)$/i,
    to: "https://$1$2.com/$3",
    regex: true
},
{
    name: "常用hosts网站2 http >> https",
    from: /^http:\/\/(web\.archive\.org|duckduckgo\.com|telegram\.org)(.*)/i,
    to: "https://$1$2",
    regex: true
},
{
    name: "常用hosts网站3 http >> https",
    from: /^http:\/\/(.*?)(github\.io)(.*)/i,
    to: "https://$1$2$3",
    regex: true
},


/*{
    name: "【https】常用网站（一）",
    from: /^http:\/\/(upload\.wikimedia\.org|t\.williamgates\.net|dyncdn\.me|www\.baidu\.com)(.*)/i,
    exclude:/^http:\/\/www\.baidu\.com\/p\//i,
    to: "https://$1$2",
    regex: true
},{
    name: "【https】常用网站（二）",
    from: /^http:\/\/(.*?)(m-team\.cc)(.*)/i,
    to: "https://$1$2$3",
    regex: true
},
*/
{
//example: http://bbs.kafan.cn/forum.php?mod=viewthread&tid=1480897&highlight=noscript
//example: http://www.zasq.net/forum.php?mod=viewthread&tid=102909&highlight=极品家丁
name:"去除URL的highlight防止卡死",
from: /^(http:\/\/.*\.(com|cn|net)\/.*?)&highlight=(.*)/,
to: "$1",
regex: true
},
/*{
name: "Google Analytics>> Minggo",
from: /^https?:\/\/(.*?)(google-analytics|googletagmanager|googletagservices|googleadservices)\.com\/([\w]+\/)*([\w]+(\.[\w]+)?)/i,
to: "http://minggo.coding.io/cdn/google/$4",
regex: true
},*/
{
name: "Gravatar头像>>多说",
from: /^https?:\/\/([0-9]?)\.gravatar\.com\/avatar\/(.*)$/,
to: "http://gravatar.duoshuo.com/avatar/$1",
regex: true
},
{ //常见网站去链接跳转: 包括zhihu.com, douban.com,mozilla.org,jump.bdimg.com
	name: "常见网站去链接跳转",
    from: /https?:\/\/.*(?:\.jobui\.com\/tips\/redirect\.php\?link=|link\.zhihu\.com\/\?target=|\.douban\.com\/link\d*\/\?url=|outgoing\.mozilla\.org\/|\.google\.com\/imgres\?imgurl=).*?(http[^&]+).*/,
    to: "$1",
	decode: true,
	regex: true
    },


];